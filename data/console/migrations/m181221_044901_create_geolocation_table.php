<?php

use yii\db\Migration;

/**
 * Handles the creation of table `geolocation`.
 */
class m181221_044901_create_geolocation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%geolocation}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'lat' => $this->float()->notNull(),
            'lng' => $this->float()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx-geolocation-user_id', '{{%geolocation}}', 'user_id');
        $this->addForeignKey('fk-geolocation-user_id', '{{%geolocation}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%geolocation}}');
    }
}
