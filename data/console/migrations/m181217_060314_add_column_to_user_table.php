<?php

use yii\db\Migration;

/**
 * Class m181217_060314_add_column_to_user_table
 */
class m181217_060314_add_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'date_of_birth', $this->date());
        $this->addColumn('{{%user}}', 'avatar', $this->string());
        $this->addColumn('{{%user}}', 'gender', $this->string());
        $this->addColumn('{{%user}}', 'subscription', $this->integer());
        $this->addColumn('{{%user}}', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'date_of_birth');
        $this->dropColumn('{{%user}}', 'avatar');
        $this->dropColumn('{{%user}}', 'gender');
        $this->dropColumn('{{%user}}', 'subscription');
        $this->dropColumn('{{%user}}', 'description');
    }


}
