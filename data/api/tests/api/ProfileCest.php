<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 18.12.2018
 * Time: 07:40
 */

namespace api\tests\api;

use \api\tests\ApiTester;
use common\fixtures\TokenFixture;
use common\fixtures\UserFixture;
use common\models\User;


class ProfileCest
{
    public function _before(ApiTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'token' => [
                'class' => TokenFixture::className(),
                'dataFile' => codecept_data_dir() . 'token.php'
            ],
        ]);
    }
    public function access(ApiTester $I)
    {
        $I->sendGET('/profile');
        $I->seeResponseCodeIs(401);
    }
    public function authenticated(ApiTester $I)
    {
        $I->amBearerAuthenticated('token-correct');
        $I->sendGET('/profile');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'id' => 1,
            'username' => 'admin',
            'email' => 'admin@gmail.com',
        ]);
        $I->dontSeeResponseJsonMatchesJsonPath('$.password_hash');
    }
    public function expired(ApiTester $I)
    {
        $I->amBearerAuthenticated('token-expired');
        $I->sendGET('/profile');
        $I->seeResponseCodeIs(401);
    }
    public function update(ApiTester $I)
    {
        $description = 'Happy New Year';
        $I->amBearerAuthenticated('token-correct');
        $I->sendPUT('/update', [
            'description' => $description,
        ]);
        $I->seeResponseCodeIs(200);
        /*$I->seeResponseContainsJson([
            'description' => $description
        ]);*/
    }

}