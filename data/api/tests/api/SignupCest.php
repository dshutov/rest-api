<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 17.12.2018
 * Time: 15:45
 */

namespace api\tests\api;
use \api\tests\ApiTester;
use common\fixtures\TokenFixture;
use common\fixtures\UserFixture;
class SignupCest
{
    public function _before(ApiTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'token' => [
                'class' => TokenFixture::className(),
                'dataFile' => codecept_data_dir() . 'token.php'
            ],
        ]);
    }


    public function badMethod(ApiTester $I)
    {
        $I->sendGET('/signup');
        $I->seeResponseCodeIs(405);
        $I->seeResponseIsJson();
    }
    public function wrongUsername(ApiTester $I)
    {
        $I->sendPOST('/signup', [
            //username is required field
            'email' => 'admin2@gmail.com',
            'password' => '12345678',
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'field' => 'username',
            'message' => 'Username cannot be blank.'
        ]);
    }
    public function uniqueEmail(ApiTester $I)
    {
        $I->sendPOST('/signup', [
            'email' => 'admin@gmail.com',
            'password' => '12345678',
            'username' => 'adm',
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'field' => 'email',
            'message' => 'This email address has already been taken.'
        ]);
    }
    public function validatePassword(ApiTester $I)
    {
        $I->sendPOST('/signup', [
            'email' => 'admin3@gmail.com',
            'password' => '12345',
            'username' => 'adm3',
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'field' => 'password',
            'message' => 'Password should contain at least 8 characters.'
        ]);
    }
    public function success(ApiTester $I)
    {
        $I->sendPOST('/signup', [
            'username' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => '12345678',
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'username' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => '12345678',
            'date_of_birth' => null,
            'avatar' => null,
            'gender' => null,
            'subscription' => null,
            'description' => null
        ]);
    }

}