<?php
return [
    [
        'id' => 1,
        'username' => 'admin',
        'auth_key' => 'd033e22ae348aeb5660fc2140aec3585',
        // password_12345678
        'password_hash' => '$2y$13$H6DUZKV5d1u/lnNwFi.jR.tGXskm9SCEmeU16ZH.xSWtpOpIn8nti',
        'password_reset_token' => null,
        'created_at' => '1545042895',
        'updated_at' => '1545042895',
        'email' => 'admin@gmail.com',
        'date_of_birth' => '1983-12-21',
        'gender' => 'man',
        'subscription' => '1',
        'description' => null,
    ],

];

