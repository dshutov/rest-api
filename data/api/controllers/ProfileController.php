<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 17.12.2018
 * Time: 10:15
 */

namespace api\controllers;

use api\models\Profile;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\filters\Cors;
use yii\filters\AccessControl;

class ProfileController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' =>  HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        return $this->findModel();
    }

    public function actionUpdate()
    {

        $model = new Profile;
        $model->load(Yii::$app->request->bodyParams, '');

        if ($model->validate()) {
            $model->upd();
            return true;
        }else{
            return $model->errors;
        }

    }

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'update' => ['put', 'patch', 'options'],
        ];
    }
    /**
     * @return User
     */
    private function findModel()
    {
        return User::findOne(Yii::$app->user->id);
    }


}