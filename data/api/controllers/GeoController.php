<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 18.12.2018
 * Time: 08:41
 */

namespace api\controllers;


use api\models\Geo;
use common\models\Geolocation;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\filters\Cors;
use yii\filters\AccessControl;

class GeoController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' =>  HttpBearerAuth::className(),
            'except' => ['options'],
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex(){
        return $this->findModel();
    }

    public function actionCreate()
    {

        $model = new Geo();
        $model->load(Yii::$app->request->bodyParams, '');

        if($model->validate()){
            $shape = $this->getShape($model);
            if($shape['message'] != "Ошибка такие значения вводить нельзя, они пересекают одну из сторон"){
                $model->create();
            }
            return $shape;
        } else {
            return $model->getErrors();
        }
    }

    public function verbs()
    {
        return [
            'index' => ['get', 'options'],
            'create' => ['post', 'options'],
        ];
    }

    private function findModel()
    {
        return Geolocation::find()->where(['user_id' => Yii::$app->user->id])->all();
    }

    function getShape($points)
    {

        $arr_points = Geolocation::find()->select(['lat', 'lng'])->where(['user_id' => 13])->all();
        array_push($arr_points, array('lat' => $points['lat'], 'lng' => $points['lng']));

        $cnt = count($arr_points);
        $cut = '';

        for ($i = 0; $i < $cnt; $i++){
            $cut += $arr_points[$i]['lng']/$arr_points[$i]['lat'];
        }

        $line = $cut/$cnt;
        if($line == 1){
            return array('message' => 'Незамкнутая фигура');
        }else {
            // С тремя точками не на одной линии должен получатся треугольник
            if ($cnt == 3) {
                $a = sqrt(pow(($arr_points[1]['lat'] - $arr_points[0]['lat']), 2) + pow(($arr_points[1]['lng'] - $arr_points[0]['lng']), 2));
                $b = sqrt(pow(($arr_points[2]['lat'] - $arr_points[1]['lat']), 2) + pow(($arr_points[2]['lng'] - $arr_points[1]['lng']), 2));
                $c = sqrt(pow(($arr_points[0]['lat'] - $arr_points[2]['lat']), 2) + pow(($arr_points[0]['lng'] - $arr_points[2]['lng']), 2));

                $type_shape = array_unique(array($a,$b,$c));
                if(count($type_shape) == 1){
                    return array('message' => 'Равносторонний треугольник');
                }elseif (count($type_shape) == 2){
                    return array('message' => 'Равнобедренный треугольник');
                }elseif (count($type_shape) == 3){
                    return array('message' => 'Разносторонний треугольник');
                }
            }
            //Расчет типа фигуры и пересечения одной из сторон четырех угольников
            if($cnt == 4){
                $a = sqrt(pow(($arr_points[1]['lat'] - $arr_points[0]['lat']), 2) + pow(($arr_points[1]['lng'] - $arr_points[0]['lng']), 2));
                $b = sqrt(pow(($arr_points[2]['lat'] - $arr_points[1]['lat']), 2) + pow(($arr_points[2]['lng'] - $arr_points[1]['lng']), 2));
                $c = sqrt(pow(($arr_points[3]['lat'] - $arr_points[2]['lat']), 2) + pow(($arr_points[3]['lng'] - $arr_points[2]['lng']), 2));
                $d = sqrt(pow(($arr_points[0]['lat'] - $arr_points[3]['lat']), 2) + pow(($arr_points[0]['lng'] - $arr_points[3]['lng']), 2));
                $ac = sqrt(pow(($arr_points[2]['lat'] - $arr_points[0]['lat']),2) + pow(($arr_points[2]['lng'] - $arr_points[0]['lng']),2));
                $db = sqrt(pow(($arr_points[1]['lat'] - $arr_points[3]['lat']),2) + pow(($arr_points[1]['lng'] - $arr_points[3]['lng']),2));
                $cbca = (($arr_points[1]['lat'] - $arr_points[2]['lat']) *($arr_points[0]['lng'] - $arr_points[2]['lng'])) - (($arr_points[0]['lat'] - $arr_points[2]['lat']) * ($arr_points[1]['lng'] - $arr_points[2]['lng']));
                $cbcd = (($arr_points[1]['lat'] - $arr_points[2]['lat']) *($arr_points[3]['lng'] - $arr_points[2]['lng'])) - (($arr_points[3]['lat'] - $arr_points[2]['lat']) * ($arr_points[1]['lng'] - $arr_points[2]['lng']));
                $abac = (($arr_points[1]['lat'] - $arr_points[0]['lat']) *($arr_points[2]['lng'] - $arr_points[0]['lng'])) - (($arr_points[2]['lat'] - $arr_points[0]['lat']) * ($arr_points[1]['lng'] - $arr_points[0]['lng']));
                $abad = (($arr_points[1]['lat'] - $arr_points[0]['lat']) *($arr_points[3]['lng'] - $arr_points[0]['lng'])) - (($arr_points[3]['lat'] - $arr_points[0]['lat']) * ($arr_points[1]['lng'] - $arr_points[0]['lng']));


                if((($cbca > 1 && $cbcd < 0) || ($cbca < 0 && $cbcd > 1)) || (($abac > 1 && $abad < 0) || ($abac < 0 && $abad > 1))){
                    return array('message' => 'Ошибка такие значения вводить нельзя, они пересекают одну из сторон');
                }

                $type_shape = array_unique(array($a,$b,$c,$d));

                if(count($type_shape) == 2 && $ac==$db){
                    return array('message' => 'Квадрат');
                }elseif (count($type_shape)==2 && $ac!=$db){
                    return array('message' => 'Ромб');
                }elseif (($ac==$db)&&($a==$c)&&($d!=$a)){
                    return array('message' => 'Прямоугольник');
                }elseif (((($arr_points[1]['lat'] - $arr_points[0]['lat'])/($arr_points[1]['lng'] - $arr_points[0]['lng']))==(($arr_points[3]['lat'] - $arr_points[2]['lat'])/($arr_points[3]['lng'] - $arr_points[2]['lng']))) || ((($arr_points[2]['lat'] - $arr_points[1]['lat'])/($arr_points[2]['lng'] - $arr_points[1]['lng']))==(($arr_points[0]['lat'] - $arr_points[3]['lat'])/($arr_points[0]['lng'] - $arr_points[3]['lng'])))){
                    return array('message' => 'Трапеция');
                }elseif (($arr_points[3]['lat'] == $arr_points[2]['lat'] && $arr_points[3]['lng'] == $arr_points[2]['lng']) || ($arr_points[3]['lat'] == $arr_points[0]['lat'] && $arr_points[3]['lng'] == $arr_points[0]['lng'])){
                    return array('message' => 'Треугольник');
                }elseif (($arr_points[3]['lat'] == $arr_points[1]['lat']) && ($arr_points[3]['lng'] == $arr_points[1]['lng'])){
                    return array('message' => 'Незамкнутая фигура');
                }
                return array('message' => 'Неправильный четырехугольник');
            }
            if($cnt > 4){
                //Для пятиугольных и больших фигур принцып расчета пересечения и типа фигур аналогичный как для четерыхугольников
                return array('message' => 'Многоугольник');
            }

        }
    }
}