<?php
namespace api\controllers;

use Yii;
use yii\rest\Controller;
use api\models\Login;
use api\models\Signup;
use yii\helpers\ArrayHelper;
use yii\filters\Cors;


/**
 * Auth controller
 */
class AuthController extends Controller
{

public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['http://localhost:3000'],
                    'Access-Control-Request-Method' => ['GET', 'HEAD', 'OPTIONS', 'POST'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ],
        ], parent::behaviors());
    }

    public function actionIndex()
    {
        return 'api';
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        $model = new Login();
        $model->load(Yii::$app->request->bodyParams, '');
        if ($token = $model->login()) {
            return $token;
        } else {
            return $model->getErrors();
        }
    }

    /**
     * Signup action.
     *
     * @return string
     */
    public function actionSignup()
    {

        $model = new Signup();
        $model->load(Yii::$app->request->bodyParams, '');

        if($model->validate()){
            $model->signup();
            return true;
        } else {
            return $model->getErrors();
        }


    }

    public function actionUpload()
    {

        if($_FILES["image"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/png"){
            move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/" . $_FILES["image"]["name"]);
            return true;
        }else{
            return false;
        }


    }

    protected function verbs()
    {
        return [
            'login' => ['post'],
            'signup' => ['post'],
            'upload' => ['post'],
        ];
    }

}
