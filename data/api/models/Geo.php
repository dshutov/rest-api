<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 18.12.2018
 * Time: 08:58
 */

namespace api\models;

use Yii;
use yii\base\Model;
use common\models\Geolocation;

class Geo extends Model
{
    public $lat;
    public $lng;


    public function rules()
    {
        return [

            [['lat', 'lng'], 'required'],
            [['lat', 'lng'], 'number'],
        ];
    }

    public function create()
    {
            $geolocation = new Geolocation();
            $geolocation->user_id = Yii::$app->user->id;
            $geolocation->lat = $this->lat;
            $geolocation->lng = $this->lng;
            $geolocation->position = '{lat: '.$this->lat.', lng: '.$this->lng.'}';
            return $geolocation->save();
    }
}