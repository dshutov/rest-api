<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 17.12.2018
 * Time: 08:19
 */

namespace api\models;


use yii\base\Model;
use common\models\User;

class Signup extends Model
{
    public $username;
    public $email;
    public $password;
    public $date_of_birth;
    public $avatar;
    public $gender;
    public $subscription;
    public $description;

    public function rules()
    {
        return [
            // username, email, password are required
            [['username', 'email', 'password'], 'required'],
            ['email', 'email'],
            [['username', 'date_of_birth', 'avatar', 'gender', 'subscription', 'description'], 'string'],
            [['email'], 'unique', 'targetClass' => 'common\models\User', 'message' => 'This email address has already been taken.'],
            [['username'], 'unique', 'targetClass' => 'common\models\User', 'message' => 'This username has already been taken.'],
            // password is validated
            ['password', 'string', 'min' => 8]
        ];
    }

    public function signup(){

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->date_of_birth = $this->date_of_birth;
        $user->avatar = $this->avatar;
        $user->gender = $this->gender;
        $user->subscription = $this->subscription;
        $user->description = $this->description;
        return $user->save();

    }

}