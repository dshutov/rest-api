<?php
/**
 * Created by PhpStorm.
 * User: SDV
 * Date: 17.12.2018
 * Time: 08:19
 */

namespace api\models;


use yii\base\Model;
use common\models\User;
use Yii;

class Profile extends Model
{
    public $username;
    public $email;
    public $password;
    public $date_of_birth;
    public $avatar;
    public $gender;
    public $subscription;
    public $description;


    public function rules()
    {
        return [
            // username, email, password are required
            [['username', 'email', 'password'], 'required'],
            ['email', 'email'],
            [['username', 'date_of_birth', 'avatar', 'gender', 'subscription', 'description'], 'string'],
            ['email', 'validateEmail'],
            ['username', 'validateUsername'],
            // password is validated
            ['password', 'string', 'min' => 8]
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {

            $users = User::find()->andWhere(['!=', 'id', Yii::$app->user->id])->andWhere(['=', 'email', $this->email])->one();

            if ($users) {
                $this->addError($attribute, 'This email address has already been taken.');
            }
        }
    }

    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {

            $users = User::find()->andWhere(['!=', 'id', Yii::$app->user->id])->andWhere(['=', 'username', $this->username])->one();

            if ($users) {
                $this->addError($attribute, 'This username has already been taken.');
            }
        }
    }

    public function upd(){

        $uid = Yii::$app->user->id;

        $user = User::find()->where(['id' => $uid])->one();

        $user->username = $this->username;
        $user->email = $this->email;
        $user->updatePassword($this->password);
        $user->date_of_birth = $this->date_of_birth;
        $user->avatar = $this->avatar;
        $user->gender = $this->gender;
        $user->description = $this->description;
        return $user->save();

    }


}