import React, {Component} from 'react';
import axios from 'axios';
import {Redirect} from "react-router-dom";
import { Button, Input, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter, Label, Container, Col, Row} from "reactstrap";
import update from 'immutability-helper';
import LeftToolbar from "../LeftToolbar/LeftToolbar";
import './Profile.css';
import {FaBars} from "react-icons/fa";
import ApiData from "../../api/ApiData";


class Profile extends Component {

    constructor(){
        super();
        this.state = {
            profile: [],
            modal: false,
            errpwd: '',
            erremail: '',
            errusername: '',
            image: '',
        };
        this.onChange = this.onChange.bind(this);
        this.onChangeImage = this.onChangeImage.bind(this);
        this.update = this.update.bind(this);
        this.toggle = this.toggle.bind(this);
    }



    componentDidMount() {

        if(sessionStorage.getItem('token')){
            const tokenarray = sessionStorage.getItem('token');
            const tokenItem = JSON.parse(tokenarray);

            axios.get(`http://dinarystest.com/profile`,{
                headers: {
                    'Authorization': 'Bearer '+tokenItem.token,
                }
            })
                .then(res => {
                    const profile = res.data;
                    this.setState({ profile });
                    this.setState({username:this.state.profile.username})
                })
        }


    }

    update() {
        if (sessionStorage.getItem('token')) {

            if(this.state.image) {
                this.fileUpload(this.state.image);
            }

            const tokenarray = sessionStorage.getItem('token');
            const tokenItem = JSON.parse(tokenarray);

            axios.put(`http://dinarystest.com/update`, this.state.profile,{
                headers: {
                    'Authorization': 'Bearer '+tokenItem.token,
                }
            })
                .then((result) => {
                    let responseJson = result;
                    if(responseJson.data === true){
                        this.toggle();
                    }else{
                        this.setState({errorpwd: responseJson.data.password});
                        this.setState({erroremail: responseJson.data.email});
                        this.setState({errorusername: responseJson.data.username});

                    }
                })
        }

    }

    onChange(e) {
        const obj = this.state.profile;
        const newObj = update(obj, {$merge: {[e.target.name]: e.target.value}});
        this.setState({profile:newObj});
        this.setState({image: e.target.files[0]});
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    onChangeImage(e){
        const obj = this.state.profile;
        const newObj = update(obj, {$merge: {[e.target.name]: e.target.files[0].name}});
        console.log({profile:newObj});
        this.setState({profile:newObj});
        this.setState({image: e.target.files[0]});
    }

    fileUpload(image){
        const url = 'upload';
        const formData = new FormData();
        formData.append('image', this.state.image, this.state.image.name);

        ApiData.post(url,formData)
    }

    render() {
        if (!sessionStorage.getItem('token')) {
            return (<Redirect to={'/'}/>)
        }
        return (

            <Row className="my_width">
                <Col md="3" className="xs_hidden">
                    <LeftToolbar />
                </Col>
                <Col xs="12" className="mobile_bars">
                    <FaBars />
                </Col>
                <Col md="9" xs="12" className="content">
                    <Container>
                        <h2 className="title_profile_form">Данные Вашего профиля:</h2>
                        <Row>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="username">Имя пользователя</Label>
                                    <Input type="text" name="username" id="username" value={this.state.profile.username} onChange={this.onChange} />
                                </FormGroup>
                                <p className="text-danger">{this.state.errorusername}</p>
                            </Col>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="email">Email</Label>
                                    <Input type="email" name="email" id="email2" value={this.state.profile.email} onChange={this.onChange} />
                                </FormGroup>
                                <p className="text-danger">{this.state.erroremail}</p>
                            </Col>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="password">Пароль</Label>
                                    <Input type="password" name="password" id="password" value={this.state.profile.password} onChange={this.onChange} />
                                </FormGroup>
                                <p className="text-danger">{this.state.errorpwd}</p>
                            </Col>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="date_of_birth">Дата рождения</Label>
                                    <Input type="date" name="date_of_birth" id="date_of_birth" value={this.state.profile.date_of_birth} onChange={this.onChange} />
                                </FormGroup>
                            </Col>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="gender">Пол</Label>
                                    <Input type="text" name="gender" id="gender" value={this.state.profile.gender} onChange={this.onChange} />
                                </FormGroup>
                            </Col>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="avatar">Фото</Label>
                                    <Input type="file" name="avatar" id="file" onChange={this.onChangeImage} />
                                </FormGroup>
                            </Col>
                            <Col md="12">
                                <FormGroup>
                                    <Label for="description">Немного о себе</Label>
                                    <Input type="textarea" name="description" id="description" value={this.state.profile.description} onChange={this.onChange} />
                                </FormGroup>
                            </Col>
                            <Col md="12">
                                <Button className="btn-primary" onClick={this.update}>Обновить данные</Button>
                            </Col>
                        </Row>
                    </Container>
                </Col>
                {/* This is begin modal window. */}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Сообщение</ModalHeader>
                    <ModalBody>
                        Ваши данные успешно обновлены.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                {/* This is end modal window. */}
            </Row>

        )
    }
}

export default Profile;