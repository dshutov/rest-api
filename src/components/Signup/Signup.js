import React, {Component} from 'react';
import ApiData from '../../api/ApiData';
import { Button, FormGroup, Input, FormText, Label } from "reactstrap";
import './Signup.css';
import logo from '../../logo.svg';
import {Redirect} from "react-router-dom";


class Signup extends Component {

    constructor(){
        super();
        this.state = {
            username: '',
            email: '',
            password: '',
            date_of_birth: '',
            gender: '',
            description: '',
            subscription: '',
            avatar: '',
            image: '',
            redirectToReferrer: false,
            erruser: '',
            errpwd: '',
            erremail: '',
        };
        this.signup = this.signup.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
        this.onChangeImage = this.onChangeImage.bind(this);
    }

    signup() {

        if(this.state.email && this.state.password){
            if(this.state.image) {
                this.fileUpload(this.state.image);
            }
          ApiData.post('signup',this.state).then((result) => {
            let responseJson = result;
            if(responseJson.data === true){
              this.setState({redirectToReferrer: true});
            }else{
                this.setState({erruser: responseJson.data.username});
                this.setState({errpwd: responseJson.data.password});
                this.setState({erremail: responseJson.data.email});
            }
          });
        }
    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }

    onChangeImage(e){
        this.setState({image: e.target.files[0]});
        this.setState({avatar: e.target.files[0].name});
    }

    fileUpload(image){
        const url = 'upload';
        const formData = new FormData();
        formData.append('image', this.state.image, this.state.image.name);

        ApiData.post(url,formData)
    }


    render() {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/'}/>)
        }
        return (

            <div className="auth_form" >
                <div className="logo">
                    <img src={logo} alt={"logo"} width="100" height="100" />
                </div>

                <div className="b_form">
                    <h2 className="login_h2">Регистрация</h2>
                    <FormGroup>
                        <Input type="text" name="username" id="username" placeholder="Имя пользователя" onChange={this.onChange} />
                    </FormGroup>
                    <span className="text-danger">{this.state.erruser}</span>
                    <FormGroup>
                        <Input type="email" name="email" id="email" placeholder="Email" onChange={this.onChange} />
                    </FormGroup>
                    <span className="text-danger">{this.state.erremail}</span>
                    <FormGroup>
                        <Input type="password" name="password" id="password" placeholder="Пароль" onChange={this.onChange} />
                    </FormGroup>
                    <span className="text-danger">{this.state.errpwd}</span>
                    <FormGroup>
                        <Input type="date" name="date_of_birth" id="date_of_birth" placeholder="Дата рождения" onChange={this.onChange} />
                    </FormGroup>
                    <FormGroup>
                        <Input type="select" name="gender" id="gender" onChange={this.onChange}>
                            <option selected disabled>Выберете пол</option>
                            <option>Мужской</option>
                            <option>Женский</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Input type="file" id="file" onChange={this.onChangeImage} />
                        <FormText color="muted">
                            Выберите фото для вашего профиля.
                        </FormText>
                    </FormGroup>
                    <FormGroup>
                        <Input type="textarea" name="description" id="description" placeholder="Немного о себе" onChange={this.onChange} />
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="checkbox" name="subscription" onChange={this.onChange} />{' '}
                            Подписатся на рассылку
                        </Label>
                    </FormGroup>
                    <Button className="btn_login" onClick={this.signup}>Зарегистрироватся</Button>

                </div>



            </div>

        )
    }
}

export default Signup;