import React, {Component} from 'react';
import LeftToolbar from "../LeftToolbar/LeftToolbar";
import './Map.css';
import {Button, Input, FormGroup, Label, Container, Col, Row, ModalHeader, ModalBody, ModalFooter, Modal} from "reactstrap";
import {FaBars} from 'react-icons/fa';
import ApiData from "../../api/ApiData";

export class MapContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lat: '',
            lng: '',
            lat_empty: '',
            lng_empty: '',
            modal: false,
            errorlat: '',
            errorlng: '',
            msg: '',
        };
        this.onChangeMap = this.onChangeMap.bind(this);
        this.toggle = this.toggle.bind(this);
        this.send = this.send.bind(this);
    }



    onChangeMap(e){
        this.setState({[e.target.name]:e.target.value});
    }

    send(){
        if(this.state.lat && this.state.lng){

            this.setState({lat_empty: ""});
            this.setState({lng_empty: ""});

            const tokenarray = sessionStorage.getItem('token');
            const tokenItem = JSON.parse(tokenarray);

            ApiData.post('create',this.state, {
                headers: {
                    'Authorization': 'Bearer '+tokenItem.token,
                }
            }).then((result) => {
                let responseJson = result;
                console.log(responseJson);
                if(responseJson.data.message !== "Ошибка такие значения вводить нельзя, они пересекают одну из сторон"){
                    this.toggle();
                    this.setState({lat: ''});
                    this.setState({lng: ''});
                    this.setState({msg: responseJson.data.message});
                }else{
                    this.setState({errorlat: responseJson.data.lat});
                    this.setState({errorlng: responseJson.data.lng});
                    this.setState({msg: responseJson.data.message});
                }
            });
        }
        else{
            if(!this.state.lat && !this.state.lng){
                this.setState({lat_empty: "lat_empty"})
                this.setState({lng_empty: "lng_empty"})
                return false
            }
            if(!this.state.lat){
                this.setState({lat_empty: "lat_empty"})
                this.setState({lng_empty: ""})
            }
            if(!this.state.lng){
                this.setState({lng_empty: "lng_empty"})
                this.setState({lat_empty: ""})
            }
        }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        return (


            <Row className="my_width">
                    <Col md="3" className="xs_hidden">
                        <LeftToolbar />
                    </Col>
                    <Col xs="12" className="mobile_bars">
                        <FaBars />
                    </Col>
                    <Col md="9" xs="12" className="points">
                        <Container>
                            <h2 className="title_geo_form">Форма ввода координат:</h2>
                            <Row>
                                <Col md="12">
                                    <FormGroup className="geo_form">
                                        <Label for="lat">Широта</Label>
                                        <Input type="text" name="lat" id="lat" value={this.state.lat} className={this.state.lat_empty} onChange={this.onChangeMap} />
                                    </FormGroup>
                                    <p className="text-danger">{this.state.errorlat}</p>
                                </Col>
                                <Col md="12">
                                    <FormGroup className="geo_form">
                                        <Label for="lng">Долгота</Label>
                                        <Input type="text" name="lng" id="lng" value={this.state.lng} className={this.state.lng_empty} onChange={this.onChangeMap} />
                                    </FormGroup>
                                    <p className="text-danger">{this.state.errorlng}</p>
                                </Col>
                                <Col md="12">
                                    <p className="text-danger">{this.state.msg}</p>
                                    <Button className="btn-primary" onClick={this.send}>Отправить</Button>
                                </Col>
                            </Row>
                        </Container>
                        </Col>

                {/* This is begin modal window. */}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Сообщение</ModalHeader>
                    <ModalBody>
                        Данные успешно отправлены на сервер.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                {/* This is end modal window. */}

                    </Row>



        );
    }
}

export default MapContainer