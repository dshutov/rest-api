import React, {Component} from 'react';

import './NotFound.css';

class NotFound extends Component {
    render() {
        return (
            <div id="Body">
                <div>
                    <h2>404 Page</h2>
                </div>
            </div>
        );
    }
}

export default NotFound;