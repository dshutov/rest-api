import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
//import { Button, Input, FormGroup, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import './LeftToolbar.css';
import Avatar from 'react-avatar';
import axios from "axios";
import {FaMapMarkerAlt, FaUser, FaSignOutAlt} from 'react-icons/fa';


class LeftToolbar extends Component {

    state = {
        user: [],
        redirectToReferrer: false,
    }

    componentDidMount() {
        if(sessionStorage.getItem('token')){
            const tokenarray = sessionStorage.getItem('token');
            const tokenItem = JSON.parse(tokenarray);

            axios.get(`http://dinarystest.com/profile`,{
                headers: {
                    'Authorization': 'Bearer '+tokenItem.token,
                }
            })
                .then(res => {
                    const user = res.data;
                    this.setState({ user });
                })
        }
    }

    componentDidUpdate() {

        if(sessionStorage.getItem('token')){
            const tokenarray = sessionStorage.getItem('token');
            const tokenItem = JSON.parse(tokenarray);

            axios.get(`http://dinarystest.com/profile`,{
                headers: {
                    'Authorization': 'Bearer '+tokenItem.token,
                }
            })
                .then(res => {
                    const user = res.data;
                    this.setState({ user });
                })
        }

    }

    logout(){
        sessionStorage.setItem("token",'');
        sessionStorage.clear();
        this.setState({redirectToReferrer: true});
    }

    render() {

        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/'}/>)
        }

        const imageUrl ="http://dinarystest.com/uploads/"+this.state.user.avatar;

        return (

            <div className="lefttoolbar">
                <Avatar name="Ivan" size="70" round={true} src={imageUrl} />
                <div className="name_profile">
                    <p>{this.state.user.username}</p>
                </div>
                <div className="line">--------------------------------------</div>
                <ul>
                    <li>
                        <a href="/profile">
                            <FaUser />
                            <span>Profile</span></a></li>
                    <li>
                        <a href="/map">
                            <FaMapMarkerAlt />
                            <span>Map</span></a></li>
                    <li>
                        <a href="/" onClick={this.logout.bind(this)}>
                            <FaSignOutAlt />
                           <span>Logout</span></a></li>
                </ul>
            </div>

        )
    }
}

export default LeftToolbar;