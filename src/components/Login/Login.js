import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import ApiData from '../../api/ApiData';
import { Button, FormGroup, Input } from "reactstrap";
import './Login.css';
import logo from '../../logo.svg';


class Login extends Component {

    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            redirectToReferrer: false,
            errorpwd: '',
            erroremail: '',
        };
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
    }


    login() {

        //if(this.state.email && this.state.password){
          ApiData.post('login',this.state).then((result) => {
            let responseJson = result;
            if(responseJson.data.token){
              sessionStorage.setItem('token',JSON.stringify(responseJson.data));
              this.setState({redirectToReferrer: true});
            }else{
                this.setState({errorpwd: responseJson.data.password});
                this.setState({erroremail: responseJson.data.email});
            }
          });
        //}
    }

    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/profile'}/>)
        }
        if (sessionStorage.getItem('token')) {
            return (<Redirect to={'/profile'}/>)
        }

        return (

            <div className="auth_form" >
                <div className="logo">
                    <img src={logo} alt={"logo"} width="100" height="100" />
                </div>

                <div className="b_form">
                   <h2 className="login_h2">Авторизация</h2>
                    <FormGroup>
                        <Input type="email" name="email" id="email" placeholder="Email" onChange={this.onChange} />
                    </FormGroup>
                    <p className="text-danger">{this.state.erroremail}</p>
                    <FormGroup>
                        <Input type="password" name="password" id="password" placeholder="Пароль" onChange={this.onChange} />
                    </FormGroup>
                    <p className="text-danger">{this.state.errorpwd}</p>
                    <Button className="btn_login" onClick={this.login}>Авторизоваться</Button>
                    <p className="text_login">Если у Вас нет учетной записи необходимо</p>
                    <a href="/signup" className="btn_signup btn">Зарегистрироватся</a>
                </div>



            </div>

        )
    }
}

export default Login;