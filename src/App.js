import React, { Component } from 'react';
import Routes from './routes';
import './App.css';


class App extends Component {

    constructor(){
        super();
        this.state={
            appName: "Simple Page Application",
            profile: false
        }
    }

  render() {
    return (
        <div className="App">
                <Routes name={this.state.appName}/>
        </div>
    );
  }
}

export default App;
