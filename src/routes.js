import React from 'react';
import {BrowserRouter,  Route,  Switch} from 'react-router-dom';

import Login from '././components/Login/Login';
import Signup from '././components/Signup/Signup';
import Profile from '././components/Profile/Profile';
import NotFound from '././components/NotFound/NotFound';
import Map from '././components/Map/Map';


const Routes = () => (
    <BrowserRouter >
        <Switch>
            <Route exact path="/" component={Login}/>
            <Route path="/profile" component={Profile}/>
            <Route path="/signup" component={Signup}/>
            <Route path="/map" component={Map}/>
            <Route path="*" component={NotFound}/>
        </Switch>
    </BrowserRouter>
);

export default Routes;