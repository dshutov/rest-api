# REST API SPA

# Данное приложение состоит из двух модулей:
- Серверная часть (REST Api) разработано на Yii2;
- SPA разработано на React js;
# Приложение REST Api создавалось на локальном сервере OpenServer, настройки сервера:
- Apache-PHP-7;
- PHP-7.0;
- MySQl-5.6 (Данная технология выбрана из-за большего опыта использования);
# Установка:
* Шаг1: Создание базы данных с именем your_name_db;
* Шаг2: Клонировать проект 
* Шаг3: cd dinarystest.com\data
* Шаг4: composer install
* Шаг5: ./init
* Шаг6: Открыть файл common\config\main-local.php
* Изменить данные БД
* 'db' => [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=your_name_db',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
],
* Шаг7: Run db migration
           Перейти в папку с проектом и запустить команду
            yii migrate

* Шаг8:
            Настроить сервер на точку входа для REST API
            Your_domain/data/api/web
			
* Шаг9: Для запуска SPA необходимо установить node js
* Шаг10: Перейти в папку с проектом и запустить команду npm install
* Шаг11: Для запуска приложения необходмио в папке с проектом запустить команду npm start
* Шаг12: Для настройки связи с REST API необходимо в файле src/api/ApiData.js, BaseURL указать ваш домен.
* Щаг13: Для корретного отображения аватарки в профиле пользователя необходимо в файле
*        src/components/LeftToolbar/LeftToolbar.js изменить путь в константе imageUrl
*        Например: const imageUrl ="http://dinarystest.com/uploads/" dinarystest.com заменить на ваш домен  
